pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
--advanced micro platformer
--@matthughson

--if you make a game with this
--starter kit, please consider
--linking back to the bbs post
--for this cart, so that others
--can learn from it too!
--enjoy! 
--@matthughson
                
--log
printh("\n\n-------\n-start-\n-------")

--config
--------------------------------

--sfx
snd=
{
    jump=0,
}

--music tracks
mus=
{

}

--math
--------------------------------

-- common comparators
function  ascending(a,b) return abs(p1.x - a.x)< abs(p1.x - b.x) end
function descending(a,b) return abs(p1.x - a.x)> abs(p1.x - b.x) end

function table_sort(arr, comp)
  if not comp then
    comp = function (a, b)
      return a < b
    end
  end
  local function partition (a, lo, hi)
      pivot = a[hi]
      i = lo - 1
      for j = lo, hi - 1 do
        if comp(a[j], pivot) then
          i = i + 1
          a[i], a[j] = a[j], a[i]
        end
      end
      a[i + 1], a[hi] = a[hi], a[i + 1]
      return i + 1
    end
  local function quicksort (a, lo, hi)
    if lo < hi then
      p = partition(a, lo, hi)
      quicksort(a, lo, p - 1)
      return quicksort(a, p + 1, hi)
    end
  end
  return quicksort(arr, 1, #arr)
end

--point to box intersection.
function intersects_point_box(px,py,x,y,w,h)
    if flr(px)>=flr(x) and flr(px)<flr(x+w) and
                flr(py)>=flr(y) and flr(py)<flr(y+h) then
        return true
    else
        return false
    end
end

--box to box intersection
function intersects_box_box(
    x1,y1,
    w1,h1,
    x2,y2,
    w2,h2)

    local xd=x1-x2
    local xs=w1*0.5+w2*0.5
    if abs(xd)>=xs then return false end

    local yd=y1-y2
    local ys=h1*0.5+h2*0.5
    if abs(yd)>=ys then return false end
    
    return true
end

function collide_player(self)
    if intersects_box_box(self.x, self.y, self.w, self.h,
                          p1.x, p1.y, p1.w, p1.h) then
        return true
    end
    return false
end

function collide_rabbit(self)
    for obj in all(objs) do
        if obj.t == 'rabbit' and intersects_box_box(self.x, self.y,
                                                    self.w, self.h,
                                                    obj.x, obj.y,
                                                    obj.w, obj.h) then
            return true
        end
    end
    return false
end

--check and see if this obj has run
--into any other objs in the objs table
--that aren't of they same type
function collide_entity(self)
    for obj in all(objs) do
        if obj.t ~= self.t then
            if intersects_box_box(self.x, self.y, self.w, self.h,
                              obj.x, obj.y, obj.w, obj.h) then
                -- deal damage to entity
                if self.t == 'sword' and obj.killable then
                    obj.health -= self.strength
                    if obj.health <= 0 then
                        obj.time_to_die = "death_by_sword"
                    end
                    self.delete = true
                end
                return true
            end
        end
    end
    return false
end

function collide_win_obj(self)
    local offset=self.w/3
    for i=-(self.w/3),(self.w/3),2 do
    --if self.dx>0 then
        if fget(mget((self.x+(offset))/8,(self.y+i)/8),7) then
            self.dx=0
            self.x=(flr(((self.x+(offset))/8))*8)-(offset)
            return true
        end
    --elseif self.dx<0 then
        if fget(mget((self.x-(offset))/8,(self.y+i)/8),7) then
            self.dx=0
            self.x=(flr((self.x-(offset))/8)*8)+8+(offset)
            return true
        end
--  end
    end
    --didn't hit a solid tile.
    return false
end

--check if pushing into side tile and resolve.
--requires self.dx,self.x,self.y, and 
--assumes tile flag 0 == solid
--assumes sprite size of 8x8
function collide_side(self)

    local offset=self.w/3
    for i=-(self.w/3),(self.w/3),2 do
    --if self.dx>0 then
        if fget(mget((self.x+(offset))/8,(self.y+i)/8),0) then
            self.dx=0
            self.x=(flr(((self.x+(offset))/8))*8)-(offset)
            return true
        end
    --elseif self.dx<0 then
        if fget(mget((self.x-(offset))/8,(self.y+i)/8),0) then
            self.dx=0
            self.x=(flr((self.x-(offset))/8)*8)+8+(offset)
            return true
        end
--  end
    end
    --didn't hit a solid tile.
    return false
end

--check if pushing into floor tile and resolve.
--requires self.dx,self.x,self.y,self.grounded,self.airtime and 
--assumes tile flag 0 or 1 == solid
function collide_floor(self)
    --only check for ground when falling.
    if self.dy<0 then
        return false
    end
    local landed=false
    --check for collision at multiple points along the bottom
    --of the sprite: left, center, and right.
    for i=-(self.w/3),(self.w/3),2 do
        local tile=mget((self.x+i)/8,(self.y+(self.h/2))/8)
        if fget(tile,0) or (fget(tile,1) and self.dy>=0) then
            self.dy=0
            self.y=(flr((self.y+(self.h/2))/8)*8)-(self.h/2)
            self.grounded=true
            self.airtime=0
            landed=true
        end
    end
    return landed
end

--check if pushing into roof tile and resolve.
--requires self.dy,self.x,self.y, and 
--assumes tile flag 0 == solid
function collide_roof(self)
    --check for collision at multiple points along the top
    --of the sprite: left, center, and right.
    for i=-(self.w/3),(self.w/3),2 do
        if fget(mget((self.x+i)/8,(self.y-(self.h/2))/8),0) then
            self.dy=0
            self.y=flr((self.y-(self.h/2))/8)*8+8+(self.h/2)
            self.jump_hold_time=0
        end
    end
end

--make 2d vector
function m_vec(x,y)
    local v=
    {
        x=x,
        y=y,
        
  --get the length of the vector
        get_length=function(self)
            return sqrt(self.x^2+self.y^2)
        end,
        
  --get the normal of the vector
        get_norm=function(self)
            local l = self:get_length()
            return m_vec(self.x / l, self.y / l),l;
        end,
    }
    return v
end

--square root.
function sqr(a) return a*a end

--round to the nearest whole number.
function round(a) return flr(a+0.5) end


--utils
--------------------------------

--print string with outline.
function printo(str,startx,
                                                             starty,col,
                                                             col_bg)
    print(str,startx+1,starty,col_bg)
    print(str,startx-1,starty,col_bg)
    print(str,startx,starty+1,col_bg)
    print(str,startx,starty-1,col_bg)
    print(str,startx+1,starty-1,col_bg)
    print(str,startx-1,starty-1,col_bg)
    print(str,startx-1,starty+1,col_bg)
    print(str,startx+1,starty+1,col_bg)
    print(str,startx,starty,col)
end

--print string centered with 
--outline.
function printc(
    str,x,y,
    col,col_bg,
    special_chars)

    local len=(#str*4)+(special_chars*3)
    local startx=x-(len/2)
    local starty=y-2
    printo(str,startx,starty,col,col_bg)
end

--objects
--------------------------------

--make the player
function m_player(x,y)

    --todo: refactor with m_vec.
    local p=
    {
        x=x,
        y=y,

        dx=0,
        dy=0,

        w=8,
        h=8,
        
        max_dx=1,--max x speed
        max_dy=2,--max y speed

        base_strength = 5,
        mod_strength = 5,
        throw_sword = false,
        windup = 1,
        jump_speed=-1.75,--jump veloclity
        acc=0.05,--acceleration
        dcc=0.8,--decceleration
        air_dcc=1,--air decceleration
        grav=0.15,
        dead = false,

        attack_button=
        {
            update=function(self)
                --start with assumption
                -- that not a new press
                self.is_pressed = false
                if btn(4) then
                    if not self.is_down then
                        self.is_pressed = true
                    end
                    self.is_down = true
                    self.ticks_down += 1
                else
                    self.is_down = false
                    self.is_pressed = false
                    self.ticks_down = 0
                end
            end,

            -- state
            is_pressed = false,
            is_down = false,
            ticks_down = 0
        },
        attack_hold_time = 0,
        min_attack_press = 5,
        max_attack_press = 15,

        -- attack_btn_released = true,
        
        --helper for more complex
        --button press tracking.
        --todo: generalize button index.
        jump_button=
        {
            update=function(self)
                --start with assumption
                --that not a new press.
                self.is_pressed=false
                if btn(5) then
                    if not self.is_down then
                        self.is_pressed=true
                    end
                    self.is_down=true
                    self.ticks_down+=1
                else
                    self.is_down=false
                    self.is_pressed=false
                    self.ticks_down=0
                end
            end,
            --state
            is_pressed=false,--pressed this frame
            is_down=false,--currently down
            ticks_down=0,--how long down
        },

        jump_hold_time=0,--how long jump is held
        min_jump_press=5,--min time jump can be held
        max_jump_press=15,--max time jump can be held

        jump_btn_released=true,--can we jump again?

        grounded=false,--on ground

        airtime=0,--time since grounded
        
        --animation definitions.
        --use with set_anim()
        anims=
        {
            ["stand"]=
            {
                ticks=1,--how long is each frame shown.
                frames={1},--what frames are shown.
            },
            ["walk"]=
            {
                ticks=2,
                frames={1,2},
            },
            ["jump"]=
            {
                ticks=1,
                frames={3},
            },
            ["slide"]=
            {
                ticks=1,
                frames={2},
            },
            ["swing"]=
            {
                ticks=3,
                frames={10,11,4}
            },
            ['dead'] = 
            {
                ticks=1,
                frames={5}
            }
        },

        curanim="walk",--currently playing animation
        curframe=1,--curent frame of animation.
        animtick=0,--ticks until next frame should show.
        flipx=false,--show sprite be flipped.
        
        --request new animation to play.
        set_anim=function(self,anim)
            if(anim==self.curanim)return--early out.
            local a=self.anims[anim]
            self.animtick=a.ticks--ticks count down.
            self.curanim=anim
            self.curframe=1
        end,
        
        --call once per tick.
        update=function(self)
    
            if self.dead then
                return true
            end
            
            --track button presses
            local bl=btn(0) --left
            local br=btn(1) --right
            
            --move left/right
            if bl==true then
                self.dx-=self.acc
                br=false--handle double press
            elseif br==true then
                self.dx+=self.acc
            else
                if self.grounded then
                    self.dx*=self.dcc
                else
                    self.dx*=self.air_dcc
                end
            end

            --limit walk speed
            self.dx=mid(-self.max_dx,self.dx,self.max_dx)
            
            --move in x
            self.x+=self.dx
            
            --hit walls
            collide_side(self)

            self.attack_button:update()
            -- similar to jump, allowed if
            if self.attack_button.is_pressed then
                self.throw_sword = true
            else
                self.throw_sword = false
                self.attack_hold_time=0
            end

            --jump buttons
            self.jump_button:update()
            
            --jump is complex.
            --we allow jump if:
            --  on ground
            --  recently on ground
            --  pressed btn right before landing
            --also, jump velocity is
            --not instant. it applies over
            --multiple frames.
            if self.jump_button.is_down then
                --is player on ground recently.
                --allow for jump right after 
                --walking off ledge.
                local on_ground=(self.grounded or self.airtime<5)
                --was btn presses recently?
                --allow for pressing right before
                --hitting ground.
                local new_jump_btn=self.jump_button.ticks_down<10
                --is player continuing a jump
                --or starting a new one?
                if self.jump_hold_time>0 or (on_ground and new_jump_btn) then
                    if(self.jump_hold_time==0)sfx(snd.jump)--new jump snd
                    self.jump_hold_time+=1
                    --keep applying jump velocity
                    --until max jump time.
                    if self.jump_hold_time<self.max_jump_press then
                        self.dy=self.jump_speed--keep going up while held
                    end
                end
            else
                self.jump_hold_time=0
            end
            
            --move in y
            self.dy+=self.grav
            self.dy=mid(-self.max_dy,self.dy,self.max_dy)
            self.y+=self.dy

            --floor
            if not collide_floor(self) then
                self:set_anim("jump")
                self.grounded=false
                self.airtime+=1
            end

            if collide_win_obj(self) then
                game_over = true
                win = true
            end

            --roof
            collide_roof(self)

            --handle playing correct animation when
            --on the ground.
            if self.grounded then
                if br then
                    if self.dx<0 then
                        --pressing right but still moving left.
                        self:set_anim("slide")
                    else
                        sfx(0)
                        self:set_anim("walk")
                    end
                elseif bl then
                    if self.dx>0 then
                        --pressing left but still moving right.
                        self:set_anim("slide")
                    else
                        sfx(0)
                        self:set_anim("walk")
                    end
                else
                    self:set_anim("stand")
                end
            end

            --flip
            if br then
                self.flipx=false
            elseif bl then
                self.flipx=true
            end

            -- attack stuff
            if self.throw_sword then
                sfx(2)
                self:set_anim('swing')
                --create the sword
                sword = make_sword(self.x, self.y, self.flipx, self.mod_strength)
                add(objs, sword)
            end

            local hit_rabbit = collide_rabbit(self)

            if hit_rabbit then
                sfx(4)
                self:set_anim('dead')
                self.dead = true
                game_over = true
                win = false
            end

            --anim tick
            self.animtick-=1
            if self.animtick<=0 then
                self.curframe+=1
                local a=self.anims[self.curanim]
                self.animtick=a.ticks--reset timer
                if self.curframe>#a.frames then
                    self.curframe=1--loop
                end
            end

        end,

        --draw the player
        draw=function(self)
            local a=self.anims[self.curanim]
            local frame=a.frames[self.curframe]
            spr(frame,
                self.x-(self.w/2),
                self.y-(self.h/2),
                self.w/8,self.h/8,
                self.flipx,
                false)
        end,
    }

    return p
end


-- ojbects in game that need some controlling

function make_rabbit(x, y)
    local rabbit = {
        x = x,
        y = y,
        flipx = true,
        dx = 0,
        dy = 0,
        w = 8,
        h = 8,
        t = 'rabbit',
        health = 5,
        killable = true,

        max_dx = 1,
        max_dy = 2,
        time_to_die = "",
        time_since_death = 0,
        delete = false,

        jump_speed=-1.75,--jump veloclity
        acc=0.05,--acceleration
        dcc=0.8,--decceleration
        air_dcc=1,--air decceleration
        grav=0.15,
        grounded=false,--on ground
        airtime=0,--time since grounded
        jumps = 0,

        anims = {
            ['still'] = {
                ticks = 1,
                frames = {6}
            },
            ['jump'] = {
                ticks = 1,
                frames = {7}
            },
            ['death_by_sword'] = {
                ticks = 60,
                frames = {8}
            }, 
            ['death_by_grenade'] = {
                ticks = 60,
                frames = {9}
            }
        },

        curanim='still',--currently playing animation
        curframe=1,--curent frame of animation.
        animtick=0,--ticks until next frame should show.
        
        ticks_since_jump = 0,

        --request new animation to play.
        set_anim=function(self,anim)
            if(anim==self.curanim)return--early out.
            local a=self.anims[anim]
            self.animtick=a.ticks--ticks count down.
            self.curanim=anim
            self.curframe=1
        end,

        update = function(self)

            -- hop towards player every 60 ticks
            --floor
            if self.time_since_death < 1 then
                if not collide_floor(self) then
                    self:set_anim("jump")
                    self.grounded=false
                    self.airtime+=1
                else
                    self.dx = 0
                    self.dy = 0
                    self.grounded = true
                end

                -- orient to face player
                if self.x < p1.x then
                    self.flipx = false
                else
                    self.flipx = true
                end

                if self.ticks_since_jump > 120 then
                    self.ticks_since_jump = 0
                    self.jumps += 1
                    if self.flipx then
                        self.dx = self.acc * 5 * -1
                    else
                        self.dx = self.acc * 5
                    end
                    self.dx *= flr(rnd(10)) + 5
                    self.dy = self.jump_speed * flr(rnd(10)) + 5
                else
                    self.ticks_since_jump += 1
                end

                if self.jumps == 2 then
                    spawn_rabbits(true)
                    self.jumps = 0
                end

                --limit walk speed
                self.dx=mid(-self.max_dx,self.dx,self.max_dx)
                
                --move in x
                self.x+=self.dx
                
                --hit walls
                collide_side(self)

                --move in y
                self.dy+=self.grav
                self.dy=mid(-self.max_dy,self.dy,self.max_dy)
                self.y+=self.dy

                --roof
                collide_roof(self)

                --handle playing correct animation when
                --on the ground.
                if self.grounded then
                    self:set_anim('still')
                end

                if self.time_to_die ~= "" then
                    self:set_anim(self.time_to_die)
                    self.time_since_death += 1
                    self.dx = 0
                end
            else
                sfx(3)
                self:set_anim(self.time_to_die)
                self.time_since_death += 1
                if self.time_since_death > 5 then
                    self.delete = true
                    score += 1
                end
            end

            --anim tick
            self.animtick-=1
            if self.animtick<=0 then
                self.curframe+=1
                local a=self.anims[self.curanim]
                self.animtick=a.ticks--reset timer
                if self.curframe>#a.frames then
                    self.curframe=1--loop
                end
            end
        end,

         draw=function(self)
            local a=self.anims[self.curanim]
            local frame=a.frames[self.curframe]
            spr(frame,
                self.x-(self.w/2),
                self.y-(self.h/2),
                self.w/8,self.h/8,
                self.flipx,
                false)
        end
    }

    return rabbit
end

function make_sword(x, y, flipx, strength )
    local velocity = 1
    if flipx then
        velocity *= -1
    end

    local sword = {
        x = x,
        y = y,
        t = 'sword',
        flipx = flipx,
        velocity = velocity,
        strength = strength,
        w = 8,
        h = 8,
        flytime = 0,
        anims = {
            ["in_air"] = 
            {
                ticks=1,
                frames={12},
            }
        },
        curanim = "in_air",
        curframe = 1,
        animtick = 0,
        delete = false,

        --request new animation to play.
        set_anim=function(self,anim)
            if(anim==self.curanim)return--early out.
            local a=self.anims[anim]
            self.animtick=a.ticks--ticks count down.
            self.curanim=anim
            self.curframe=1
        end,

        update = function(self)
            self.x += self.velocity
            --hit wals
            local collision = false
            if collide_side(self) then
                collision = true
            elseif collide_entity(self) then
                collision = true
            else
                collision = false
            end

            self.delete = collision
            self:set_anim('in_air')

            --anim tick
            self.animtick-=1
            if self.animtick<=0 then
                self.curframe+=1
                local a=self.anims[self.curanim]
                self.animtick=a.ticks--reset timer
                if self.curframe>#a.frames then
                    self.curframe=1--loop
                end
            end

        end,

        draw=function(self)
            local a=self.anims[self.curanim]
            local frame=a.frames[self.curframe]
            spr(frame,
                self.x-(self.w/2),
                self.y-(self.h/2),
                self.w/8,self.h/8,
                self.flipx,
                false)
        end
    }

    return sword
end

function make_handgrenade(x, y)
    local handgrenade = {

        x = x,
        y = y,
        killable = false,
        strength = 200,
        health = 0,
        w = 8,
        h = 8,
        t = 'grenade',

        anims = {
            ['still'] = {
                ticks = 1,
                frames = {13}
            },
            ['boom'] = {
                ticks = 1,
                frames = {14}
            }
        },
        curanim = 'still',
        curframe = 1,
        animtick = 0,
        delete = false,

        --request new animation to play.
        set_anim=function(self,anim)
            if(anim==self.curanim)return--early out.
            local a=self.anims[anim]
            self.animtick=a.ticks--ticks count down.
            self.curanim=anim
            self.curframe=1
        end,

        update = function(self)
            if collide_player(self) then
                self.delete = true
                sfx(5)
                self:set_anim('boom')
                cam:shake(15,2)
                for obj in all(objs) do
                    if obj.t == 'rabbit' then
                        obj.health -= self.strength
                        if obj.health <= 0 then
                            obj.time_to_die = "death_by_grenade"
                        end
                    end
                end
            else
                self:set_anim('still')
            end

            --anim tick
            self.animtick-=1
            if self.animtick<=0 then
                self.curframe+=1
                local a=self.anims[self.curanim]
                self.animtick=a.ticks--reset timer
                if self.curframe>#a.frames then
                    self.curframe=1--loop
                end
            end

        end,

        draw=function(self)
            local a=self.anims[self.curanim]
            local frame=a.frames[self.curframe]
            spr(frame,
                self.x-(self.w/2),
                self.y-(self.h/2),
                self.w/8,self.h/8,
                self.flipx,
                false)
        end

    }

    return handgrenade
end

-- spawn random number of rabbits within the bounds
-- within a certain window around the player
function spawn_rabbits(force_spawn)
    local current_num_rabbits = 0
    if force_spawn == nil then
        force_spawn = false
    end
    for obj in all(objs) do
        if (obj.t == 'rabbit') current_num_rabbits += 1
    end

    -- if current number of rabbit is less than 5, spawn one maybe
    if current_num_rabbits < 5 or force_spawn then
        -- window will be player +- 8 to screen edge
        local upper_x = flr(rnd(p1.x + 120)) + p1.x + 8
        -- if (upper_x > 128) upper_x = 112
        local lower_x = flr(rnd(p1.x)) - 8
        if (lower_x < 8) lower_x = 16
        local x = 0
        if rnd(1) > .2 then -- bias to spawn in front of 
            x = upper_x
        else
            x = lower_x
        end

        local y = p1.y -- always spawn 16 below top of screen

        -- don't let the spawn on top of you
        if abs(x -p1.x) < 16 then
            return false
        end

        add(objs, make_rabbit(x, y))
    end
end


-- delete any rabbits > 128 away
function clean_rabbits()
    for obj in all(objs) do
        if obj.t == 'rabbit' and abs(obj.x - p1.x) > 256 then
            obj.delete = true
        end
    end
end

-- update every tick
function update_objs()
    for obj in all(objs) do
        obj:update()
        if obj.delete then
            del(objs, obj)
        end
    end
end

function draw_objs()
    for obj in all(objs) do
        obj:draw()
    end
end

--make the camera.
function m_cam(target)
    local c=
    {
        tar=target,--target to follow.
        pos=m_vec(target.x,target.y),
        
        --how far from center of screen target must
        --be before camera starts following.
        --allows for movement in center without camera
        --constantly moving.
        pull_threshold=16,

        --min and max positions of camera.
        --the edges of the level.
        -- pos_min=m_vec(64,64),
        -- pos_max=m_vec(320,64),
        pos_min=m_vec(64,64),
        pos_max=m_vec(1024,256),
        shake_remaining=0,
        shake_force=0,

        update=function(self)

            self.shake_remaining=max(0,self.shake_remaining-1)
            
            --follow target outside of
            --pull range.
            if self:pull_max_x()<self.tar.x then
                self.pos.x+=min(self.tar.x-self:pull_max_x(),4)
            end
            if self:pull_min_x()>self.tar.x then
                self.pos.x+=min((self.tar.x-self:pull_min_x()),4)
            end
            if self:pull_max_y()<self.tar.y then
                self.pos.y+=min(self.tar.y-self:pull_max_y(),4)
            end
            if self:pull_min_y()>self.tar.y then
                self.pos.y+=min((self.tar.y-self:pull_min_y()),4)
            end

            --lock to edge
            if(self.pos.x<self.pos_min.x)self.pos.x=self.pos_min.x
            if(self.pos.x>self.pos_max.x)self.pos.x=self.pos_max.x
            if(self.pos.y<self.pos_min.y)self.pos.y=self.pos_min.y
            if(self.pos.y>self.pos_max.y)self.pos.y=self.pos_max.y
        end,

        cam_pos=function(self)
            --calculate camera shake.
            local shk=m_vec(0,0)
            if self.shake_remaining>0 then
                shk.x=rnd(self.shake_force)-(self.shake_force/2)
                shk.y=rnd(self.shake_force)-(self.shake_force/2)
            end
            return self.pos.x-64+shk.x,self.pos.y-64+shk.y
        end,

        pull_max_x=function(self)
            return self.pos.x+self.pull_threshold
        end,

        pull_min_x=function(self)
            return self.pos.x-self.pull_threshold
        end,

        pull_max_y=function(self)
            return self.pos.y+self.pull_threshold
        end,

        pull_min_y=function(self)
            return self.pos.y-self.pull_threshold
        end,
        
        shake=function(self,ticks,force)
            self.shake_remaining=ticks
            self.shake_force=force
        end
    }

    return c
end

--game flow
--------------------------------

--reset the game to its initial
--state. use this instead of
--_init()
function reset()
    game_over = false
    win = false
    score = 0
    objs = {}
    ticks=0
    p1=m_player(16,16)
    p1:set_anim("walk")
    add(objs, make_handgrenade(24, 48)) -- at start
    add(objs, make_handgrenade(544, 88))
    add(objs, make_handgrenade(216, 8))
    add(objs, make_handgrenade(968, 16))
    add(objs, make_handgrenade(416, 96))
    cam=m_cam(p1)
end

--p8 functions
--------------------------------

function _init()
    music(0)
    reset()
end

function _update60()
    if not game_over then
        ticks+=1
        p1:update()
        cam:update()
        update_objs()
        table_sort(objs, ascending)
        spawn_rabbits()
        clean_rabbits()
    else
        if (btnp(5)) reset()
    end
end

function _draw()

    cls(0)
    local c_x, c_y = cam:cam_pos()
    camera(c_x, c_y)
    
    map(0,0,0,0,128,32)
    
    p1:draw()
    draw_objs()
    print("score: ".. score,c_x, c_y, 7)
    if game_over then
        if win then
            print('you win!', c_x+32, c_y + 8)
        else
            print("you died!", c_x+32, c_y + 8, 8)
        end
        print('press x to play again', c_x + 32, c_y + 16, 8)
    end
    --hud
    camera(0,0)
end
__gfx__
00000000706660007066600000666400066600007866600800000700000070000000070090090800006667000066600000000000000aa000a909a00a00000000
0000000070660000706600000766004006600000888888000000070000007780000088009780878000660070006600000000000008aaaa8008aaaa9900000000
00700700706660047066600470666004066600007868608e000007000070777780000e0000909080406660074066600700000000000aa000a07aa00000000000
0007700073383304733833044338336433833000833e888070007780000777008e8e78808a0788704338336443383370040000000a8998a0098799aa00000000
00077000738883647388836403888300388830008888838e0777777700077070088788780907a9904388830043888340447777778a9889a88a97797800000000
00700700433833044338330403383300338347778e3833e007777700007700070778870087888078433833004338330004000000aa9889aaaa9889a700000000
00000000066666040666660406666600666660000e668e00070000700700000008e80e88909889884666660046666600000000000a8998a0978998a000000000
00000000050005000050005000505000500050000888050007777007700000000887700808aa000a00505000005050000000000000aaaa0090aaa90900000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
3b3333335565666554645446ccccccccccccccccc4c4c4c400000000a009900a0000000055555555555550000000000000000000000000000000000000000000
3333b3b36566555544464444ccccccccccc77ccc4c4c4c4c00000000009999000000000055555555555550000000000000000000000000000000000000000000
444444445656655645444454ccccccccccc777ccc4c4c4c405050505a099990a0000000555555555555555000000000000000000000000000000000000000000
444644446666555644464444ccccccccc7777777cccccccc00999555004444000000000555aaa555aaa555000000000000000000000000000000000000000000
454445446565666645444544ccccccccc7177711cccccccc059995550a0440a00000000555aaa555aaa555000000000000000000000000000000000000000000
464464446555555546446444cccccccccc711177cccccccc44444444000440000000000555aaa555aaa555000000000000000000000000000000000000000000
444444445556666644444444ccccccccccc7777ccccccccc04000400a004400a0000000555555555555555000000000000000000000000000000000000000000
454444456665565545444445cccccccccccccccccccccccc84888488000440000000000055555555555550000000000000000000000000000000000000000000
55655655040404040060000006000000aaaaaaaa5655555500000000000000000000000055555555555550000000000000000000000000000000000000000000
666666664040404050000a05000500008a8aa8a80555655000000000000000000000000000555555555000000000000000000000000000000000000000000000
55655655040404040a005000050000050aaaaaa00565550000000000000000000005555555555555555555555500000000000000000000000000000000000000
5565565500000000050000000000000000aaaa000555550000000000000000000005555555555555555555555500000000000000000000000000000000000000
55655655000000000000500600006000000aa0000055500000000000000000000005555555555555555555555500000000000000000000000000000000000000
55655655000000000600000005000006000880000055500000000000000000000005555555555555555550555500000000000000000000000000000000000000
6666666600000000005000a00000000000aaaa000005600000000000000000000005555555558888855550555500000000000000000000000000000000000000
55655655000000000000a000006000500a8aa8a00000500000000000000000000005555055558888855550555500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005555055558888855550555500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005555055558888855550555500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005555055558888855550555500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005555055558888855550555500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005550005558888855500055500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005550005555555555500055500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000005550005555555555500055500000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000005555555555500000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000005550000055500000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000005550000055500000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000005550000055500000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000005550000055500000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000005550000055500000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000055555550000055555550000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000055555550000055555550000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000055555550000055555550000000000000000000000000000000000000000
__gff__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001010100000200000000000000000000010200008001000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
4143434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343430000000000000000000000000000000000410000
4144434343434343434343434343434343434443434343434343434343434343434343434343434343434443434343434343434343444343444343434343434343434443434343434343434343434343434343434343444344434343434343434344434343434344434343430000000000000000000000470000470000410000
4143434345454543434343434443434343434343434343444345454544434343434343434343434343434343454545454343434343434343434343434343434343434343434343434343434343434343434343434343434343434343434345454545454543434343434343430000000000000000000046000000000000410000
4143434343434343434443434343434343434443434343434343434343434343434343434343434343434343444343434343434343434343434343434343434343434343434343434343434343434344434343434343434343434343434343434343434343434343434343430000000000000000004141414141410000410000
4143434344434343434343434343434343434343454545434343434344434343434343434340404040434343434343434343454545454543434343434343434343434343434343434343434343434343434343434343434343434343434343434343434343444345454545450000000000000000414141414141410000410000
4143434343434343434545454343434343434343434343434343434343434343434343434042424242404343434343434343434343434343434343434343434343434343434344434343434343434343434343434343434343434343434343454545454343434343434343430000000000000041414141414141410000410000
4143434343434343434343434343434340404040404040404040404040404040434343404242424242424040404040404040404040404040404040404343434343404040404040404343434343434040404040404040404040404040404043434343434343434343434343430000000000004141414141414141410000410000
4140404040404040404040404040404042424242424242424242424242424242404040424242424242424242424242424242424242424242424242424243434342424242424242424242434342424242424242424242424242424242424242424242424242424242424242424242424242414141414141414141410000410000
4141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414100000041414141414141414141434341414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141410000410000
4100525252520053000000000000000000000000000000005300000000000000000000000000000000000000000000000000000000000000000000005000515100000000000000000000000050000055000055000000550053000000000000000000000000000000000000000000000000000000000000000000000000410000
4100545252525300530053530000000000000000000000000000000000000000000000515151000000000053000000520000000000000000005200005000000047000000470000000047005150000053000000520000000000000000000053000052000000000000000052000000000000000000530000000000000000410000
4100505252520000000000530051515151000000530053000000000000005200000000000000000000000000000000000000000052000000000000005000000000000000000000000000000050000000000000000000000000000000520000000000004700000047000000000000000052000000000000000052000000410000
4100505050500053530000000000000000000000000000000000000000004700000000000000000000000000000000000047000000000000470052005050505050505050505050505050505050000000000053000047000000470000000000000000000000460000000000515151000000000000000000000000000000410000
4100505050505050505050500000000000000000000000000000005200000000005200000053000051515100000000520050000000000000500000000000000000000000000050000000005500000000530000000000000000000000005151510000000000505000000000000000000051515148494a51515100000000410000
4100505050505050505050500000515151510000000000000000000000505050500000000000000000005300000000000050504700004750500000005300000000000000530000000053000000000000000000000000000000000000000000000052000050505050000000005200000000470058595a5b470000000000410000
4100505050505050505050500000000053000000000000530000000000505050500000000000000000000000515100000050505046465050500000000000000000005000000000000000000000000041000000000050505050500000000000000000005050505050505050000000000000000068696a6b000000530052410000
4141414150504141415050500000000000000041414100000000000050505050505000000000000000000000000000000050505050505050500000005000000000005000000041000000004100000041000000000050505050505000000000000000505050505050505050000000000000000078797a7b000000000000410000
4141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141410000
__sfx__
000100000203005030050300302001020010000140003400094000240004400084000e00003400034000340003400034000340003400034000340000000000000000000000000000000000000000000000000000
0003000001050010500205005050080500c0301102018010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000100000000000000000000000000000000001e710267302f730377502f730257301d71000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000100002425021250202501c250192501823015230122300f2200d2200a210072100321001200012000220001200012000120001200012000120001200012000120001200012000220001200012000120001200
00050000000003b250392503625033250312502e2502c2502a25028250262502425021250202501d2501a250182501525012250102500d2500b25009250062500374003700017000170001700017000170001700
00020000065500755007550095500a5500d5501055014550175501a5501e55022550285502f550365503b5503e5503e55039550335502a550225501a55014550105500e5500c5500b55009550085500655006550
012000002e0302e0302e0302e0302e03029045290452e0302c0302a0302c0302c0302c0302c0302c0302c0302e0302e0302e0302e0302e0302a0452a0452e0302d0302b0302d0302d0302d0302d0302d0302d030
0120000016050160501d0501d0502205022050220502205014050140501b0501b05020050200502005020050120501205019050190501e0501e0501e0501e050110501105018050180501d0501d0501d0501d050
011000001d0501d0001d050160501d0501d0001d050160501d0501d0001d050160501d050160501d050160501d0501d0001d050160501d0501d0001d050160501d0501d0001d050160501d050160501d05016050
01100000160551d0051605516055160551d0051605516055160551d000160551605516055160551605516055160551d0001605516055160551d0051605516055160551d005160551605516055160551605516055
01100000220502205022050220501d0501d0501d0501d0501d0501d00022070220002207024070260702707029050290502905029050220502405026050270502905029050290502905029050290502905029050
011000001d0501d0001d050160501d0501d0001d050160501d0501d0001d050160501d050160501d050160501b0501d0001407514075140751d0051407514075140751d0001b050140501b050140501b05014050
01100000190501d0001905012050190501d0001905012050190501d000190501205019050120501905012050180501d0001107511075110751d0051107511075110751d000180501105018050110501805011050
011000001605000000160751607516075000001607516075160750000016075160751607500000160751607514050000001407514075140750000014075140751407500005140751407514075000001407514075
011000001205012000120751207512075000001207512075120750000012075120751207500000120751207519050000001907519075190750000019075190751907500005190751907519075000001907519075
011000001705012000170751707517075000001707517075170750000017075170751707500000170751707516050000001607516075160750000016075160751607500005160751607516075000001607516075
01100000180501200018075180751807500000180751807518075000001807518075180750000018075180751d050000001107511075110750000011075110751107500005110751107511075000001307515075
011000001205012000120751207512075000001207512075120750000012075120751207500000120751207511050000001107511075110750000011075110751107500005110751107511075000001107511075
011000001005012000100751007510075000001007510075100750000010075100751007500000100751007511050000001107511075110750000011075110751107500005110751107511075000001107511075
01100000220502205022050220501d0501d0501d0501d0501d0501d000220701d0002207024070260702707029050290502905029050220502405026050270502905029050290502900029050290502a0502c050
011000002e0502e0502e0502e0502e0502e0502e0502e0502e0002e0002e0702e0002e0702e0702c0702a0702c0502c0502c0502a050290502905029050290502905029050290002900029050290502905029050
01100000270502700027050290502a0502a0502a0502a0502a0502a0502a0002a0002905029050270502705025050250002505027050290502905029050290502905029050290002900027050270502505025050
01100000240502400024050260502805028050280502805028050280502a0002a0002b0502b0502b0502b05029050190001d0751d0751d0751d0001d0751d0751d0751d0001d0751d0751d0751d0001d0501d050
011000002e0502e0502e0502e0502e0502e0502e0502e0502e0502e0502e0502e05031050310503105031050300503005030050300502c0502c0502c0502c0502c0502c0502c0502c05029050290502905029050
01100000100501005016050190501c0501c0502205025050280502805028050280502e0502e0502e0502e0502d0502d0502d0502d050290502905029050290502905029050290002900029050290502905029050
011000002705027050270502705027050270502705027050270502705027050270502a0502a0502a0502a05029050290502905029050250502505025050250502505025050250502505022050220502205022050
011000002e0002e0002e0002e0002e0002e0002e0002e0002e0002e0002e0002e000310003100031000310001d0701d0001d0751d070210701500021070240701800024070210701500021075210702105021030
011000002e0002e0002c0002c0002e0002e00029000290002e0002e000300003000031000310003000030000002430a600006302c600306352e6000024329600002430a600006302c600306352e6000024329600
010c00001865318667186771865718665186771865718667186771865518667186771865718667186751865718667186771865718665186771865718667186771865518667186771865718647186371862718617
010300003072316203132000f200082051d4000a2032c0000a203080000a600080002e305080000a2032c000082030700008600070002c305080000820307000082030700008600070002c305080000820307000
011000002e0302e0302e0302e0302905029050290502905035030350303503035030290502905029050290502c0302c0302c0302c030270502705027050270503303033030330303303027050270502705027050
0110000022030220302203022030190501905019050190501d0301d0301d0301d03022050220502205022050210302103021030210301d0501d0501d0501d050290302903029030290301d0501d0501d0501d050
01100000002430a600006302c600306352e6000024329600002430a600006302c600306352e6000024329600002430a600006302c600306352e6000024329600002430a600006302c600306352e6000024329600
011000001c0501c05028060280601c0501c05028050280501c0501c05028060280601c0501c05028050280501d0501d05029060290601d0501d05029050290501d0501d05029060290601d0501d0502905029050
01100000280501c0002805034000280501c0002805034000340301c0002805034000280501c000280503400029050350002905035000290503500029050350003503035000290503500029050350002905035000
01100000330303303033030330302e0302e0302e0302e030360303603036030360303603036030360203601019070190701907019070160701607016070160701d0701d0701d0701d0701d0501d0401d0301d020
011000002405024050240502405024050240502405024050210402104021040210402104021030210202101027005270052b005270052b0052b00527005270051b00526005240052500526005200052300524005
01100000002430a600006302c600306352e6000024329600002430a600006302c600306352e6000024329600002030a600006002c600306052e6000020329600002030a600006002c600306052e6000020329600
__music__
01 06074344
00 09084344
00 04054344
00 0a0c4344
00 130d4344
00 140e4344
00 150f4344
00 16101a1b
00 150d1e20
00 17111f20
00 18122120
00 18122220
00 190f2320
02 16102425

